#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  if not os.path.exists('config'):
    os.makedirs('config')

  print("Setting up mailuser parameters...")
  print("\n")
  mastodon_db = input("Mastodon db name: ")
  mastodon_db_user = input("Mastodon db user: ")
  mailusers_db = input("mailusers db name: ")
  mailusers_db_user = input("mailusers db user: ")
  with open(file_path, "w") as text_file:
      print("mastodon_db: {}".format(mastodon_db), file=text_file)
      print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)
      print("mailusers_db: {}".format(mailusers_db), file=text_file)
      print("mailusers_db_user: {}".format(mailusers_db_user), file=text_file)

def create_table(db, db_user, sql):

    try:

        conn = None
        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
        cur = conn.cursor()


        # Create the table in PostgreSQL database
        cur.execute(sql)

        conn.commit()

    except (Exception, psycopg2.DatabaseError) as error:

        print("\n")
        print(error)

    finally:

        if conn is not None:

            conn.close()

#############################################################################################

# Load configuration from config file
config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
mailusers_db = get_parameter("mailusers_db", config_filepath)
mailusers_db_user = get_parameter("mailusers_db_user", config_filepath)

############################################################
# create database
############################################################

conn = None
try:

    conn = psycopg2.connect(dbname='postgres',
        user=mailusers_db_user, host='',
        password='')

    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cur = conn.cursor()

    print("Creating database " + mailusers_db + ". Please wait...")
    cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(mailusers_db))
        )
    print("Database " + mailusers_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()


#############################################################################################

try:

    conn = None
    conn = psycopg2.connect(database = mailusers_db, user = mailusers_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.OperationalError) as peer_error:

    # Load configuration from config file
    os.remove("config/db_config.txt")
    print("1. As root, add " + mailusers_db + " and " + mailusers_db_user + " to Potsgresql's pg_hba.conf config file (IPv4 section) ")
    print("and set METHOD to 'trust'. Then restart Postgresql service.")
    print("2. Rerun `python db-setup.py")
    print("\n")
    sys.exit(0)

except (Exception, psycopg2.DatabaseError) as error:

    print(error)
    # Load configuration from config file
    os.remove("config/db_config.txt")
    print("Exiting. Run setup again with right parameters")
    sys.exit(0)

if conn is not None:

    print("\n")
    print("mailusers db parameters saved to config.txt!")
    print("\n")

############################################################
# Create needed tables
############################################################

db = mailusers_db
db_user = mailusers_db_user
sql = "create table mailusers(datetime timestamptz, account_id int primary key, username varchar(30), email varchar(50), emailed_at timestamptz,"
sql += "emailed boolean default False)"
create_table(db, db_user, sql)

#####################################

print("mailusers DB setup done!")
print("Now you can run setup.py!")
print("\n")
