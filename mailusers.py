#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timezone, timedelta
import time
import threading
import os
import sys
import os.path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from smtplib import SMTPException, SMTPAuthenticationError, SMTPConnectError, SMTPRecipientsRefused
import psycopg2
import socket
from socket import gaierror

###################################################################################
# write to database mailing status of users

def write_db(now, id, username, email, emailed_at, emailed):

    insert_line = "INSERT INTO mailusers(datetime, account_id, username, email, emailed_at, emailed) VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

    conn = None

    try:

        conn = psycopg2.connect(database = mailusers_db, user = mailusers_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("SELECT account_id FROM mailusers where account_id=(%s)", (id,))

        row = cur.fetchone()

        if row == None:

            cur.execute(insert_line, (now, id, username, email, now, emailed))

        else:

            return

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        if file_path == "secrets/secrets.txt":
          print("File %s not found, exiting. Run setup.py."%file_path)
        elif file_path == "config.txt":
          print("File %s not found, exiting. Run db-setup.py."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    print("Run python setup.py")
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
smtp_host = get_parameter("smtp_host", secrets_filepath)
smtp_user_login = get_parameter("smtp_user_login", secrets_filepath)
smtp_user_password = get_parameter("smtp_user_password", secrets_filepath)
email_subject = get_parameter("email_subject", secrets_filepath)

# Load configuration from config file
config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
mailusers_db = get_parameter("mailusers_db", config_filepath)
mailusers_db_user = get_parameter("mailusers_db_user", config_filepath)

##########################################################################################
# main

now = datetime.now()

##########################################################################################
# Connect to Mastodon's Postgresql database to get all users's  email address and username
##########################################################################################

conn = None

try:

    conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("select email, username, account_id from accounts, users where users.account_id = accounts.id and users.approved and accounts.suspended_at is null order by account_id")
    rows = cur.fetchall()

    users_email = []
    users_username = []
    users_id = []

    for row in rows:

        users_email.append(row[0])
        users_username.append(row[1])
        users_id.append(row[2])

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

##########################################################################################################
# check if current emailing is finished

conn = None

try:

    conn = psycopg2.connect(database = mailusers_db, user = mailusers_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("SELECT count(account_id) from mailusers")

    row = cur.fetchone()

    if row != None:

        if row[0] == len(users_email):
            print("\n")
            print("Current mailing is already finished!")
            print("Run 'python newmailing.py' if you want start a new one!")
            print("\n")
            sys.exit(0)

    cur.close()

except (Exception, psycopg2.DatabaseError) as error:

    print(error)

finally:

    if conn is not None:

        conn.close()

###########################################################################################################
#  send email
###########################################################################################################

try:

    fp = open('message.txt')
    text = fp.read()
    message = MIMEText(text)
    fp.close()

except:

    print("message.txt not found! Create it and write in it the body of your email for your users.")
    sys.exit(0)

i = 0
while i < len(users_email):


      conn = None

      try:

          conn = psycopg2.connect(database = mailusers_db, user = mailusers_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          cur.execute("SELECT account_id FROM mailusers where account_id=(%s)", (users_id[i],))

          row = cur.fetchone()

          if row == None:

              emailed = True
              write_db(now, users_id[i], users_username[i], users_email[i], now, emailed)

          else:

              i += 1
              continue

          cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

          print(error)

      finally:

          if conn is not None:

              conn.close()

      msg = MIMEMultipart()

      msg['From'] = smtp_user_login
      msg['To'] = users_email[i]
      msg['Subject'] = users_username[i] + " " + email_subject

      msg.attach(message)

      try:

          # Create the server connection
          server = smtplib.SMTP(smtp_host)
          # Switch the connection over to TLS encryption
          server.starttls()
          # Authenticate with the server
          server.login(smtp_user_login, smtp_user_password)
          # Send the message
          server.sendmail(msg['From'], msg['To'], msg.as_string())
          # Disconnect
          server.quit()

          print(str(i+1) + " of " + str(len(users_email)) + ": email sent to " + users_username[i] + ", %s" % msg['To'])

          i += 1

          time.sleep(2)

      except SMTPAuthenticationError as auth_error:

          print(auth_error)
          sys.exit(":-(")

      except socket.gaierror as socket_error:

          print(socket_error)
          print("Unknown SMTP server")
          sys.exit(":-(")

      except SMTPRecipientsRefused as recip_error:

          print(recip_error)
          i += 1

