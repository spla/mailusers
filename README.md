# mailusers

Mail all your Mastodon server users.
This code written in Python get all your Mastodon server users email addresses from the database and email them with the subject and message of your choice.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon server (admin)

### Usage:

Within Python Virtual Environment:

1. Run `python db-setup.py` to set up your Mastodon server database parameters and to create new database for mailusers.

2. Run `python setup.py` to set your SMTP server parameters and desired email subject. They will be saved to 'secrets/secrets.txt' for further use.

3. Run `python mailusers.py` to email all your users.

4. Run `python newmailing.py` to delete all data from mailusers's database and run `python mailusers.py` again to start a new mailing,
 
Note: install all needed packages with 'pip install -r requirements.txt'
