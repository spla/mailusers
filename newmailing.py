#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timezone, timedelta
import time
import os
import sys
import os.path
import psycopg2

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        if file_path == "secrets/secrets.txt":
          print("File %s not found, exiting. Run setup.py."%file_path)
        elif file_path == "config/db_config.txt":
          print("File %s not found, exiting. Run db-setup.py."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    print("Run python setup.py")
    sys.exit(0)

# Load configuration from config file
config_filepath = "config/db_config.txt"
mastodon_db = get_parameter("mastodon_db", config_filepath)
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath)
mailusers_db = get_parameter("mailusers_db", config_filepath)
mailusers_db_user = get_parameter("mailusers_db_user", config_filepath)

##########################################################################################
# main

##########################################################################################
#  ask what to do

while True:

    newmailing = input("Start new all users mailing? (y/n)")

    if newmailing == 'y':

        print("Deleting all " + mailusers_db + " rows to start over." )

        conn = None

        try:

            conn = psycopg2.connect(database = mailusers_db, user = mailusers_db_user, password = "", host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("delete from "+ mailusers_db)

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

            print("Now you can start a new all users mailing by running 'python mailusers.py'")

            sys.exit("Done.")

    elif newmailing == 'n':

        sys.exit("Ok, bye")

